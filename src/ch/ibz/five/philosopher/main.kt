package ch.ibz.five.philosopher

fun main(args: Array<String>) {

    val philosopher = listOf("P1", "P2", "P3", "P4", "P5").map { Philosopher(it) }
    fun buildStatus() = philosopher.map { "${it.status.color.code}${it.name}[${it.status.text}]" }.joinToString(" ")

    philosopher.forEach { philosopher -> Thread(philosopher).start() }
    while (true) {
        print("\r\r${buildStatus()}")
        Thread.sleep(50)
    }

}
