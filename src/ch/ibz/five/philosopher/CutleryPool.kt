package ch.ibz.five.philosopher

import java.util.*

class Cutlery(val number: Int)

object CutleryPool {

    private var cutleryMap = (1..5).map { i -> Cutlery(i) }.associateBy({ it.number }, { it }).toMutableMap()
    private val totalOfCutlery = cutleryMap.size

    fun getCutlery(philosopher: Philosopher): Optional<List<Cutlery>> {
        val ownCutlery = philosopher.number
        val neighborCutlery = when (ownCutlery) {
            1 -> totalOfCutlery
            else -> ownCutlery - 1
        }

        synchronized(cutleryMap) {
            if (cutleryMap.containsKey(ownCutlery) && cutleryMap.containsKey(neighborCutlery)) {
                val preparedCutlery = selectCutlery(ownCutlery, neighborCutlery)
                removeCutleryFromPool(preparedCutlery)
                return Optional.of(preparedCutlery)
            }
        }

        return Optional.empty()
    }

    fun putCutlery(cutlery: List<Cutlery>) {
        cutlery.forEach { c -> cutleryMap.put(c.number, c) }
    }

    private fun selectCutlery(ownCutlery: Int, neighborCutlery: Int) = listOf(cutleryMap.getValue(ownCutlery), cutleryMap.getValue(neighborCutlery))

    private fun removeCutleryFromPool(cutlery: List<Cutlery>) {
        cutlery.forEach { c -> cutleryMap.remove(c.number) }
    }

}