package ch.ibz.five.philosopher

import java.util.*

enum class Status(val text: String, val color: Color) {

    RESERVE_CUTLERY("RESERVE-CUTERLY", Color.TXT_CYAN),
    EATING("------EAT------", Color.TXT_GREEN),
    RELEASE_CUTLERY("RELEASE-CUTERLY", Color.TXT_BLUE),
    SLEEP("-----SLEEP-----", Color.TXT_RED)

}

data class Philosopher(val name: String) : Runnable {

    val number = name.last().toString().toInt()
    var status = Status.SLEEP
    var cutleryList: Optional<List<Cutlery>> = Optional.empty()

    override fun run() {
        while (true) {
            reserveCutlery()
            eat()
            releaseCutlery()
            sleep()
        }
    }

    private fun reserveCutlery() {
        status = Status.RESERVE_CUTLERY
        while (!cutleryList.isPresent) {
            cutleryList = CutleryPool.getCutlery(this)
            Thread.sleep(100)
        }
    }

    private fun eat() {
        status = Status.EATING
        sleepForAWhile()
    }

    private fun releaseCutlery() {
        status = Status.RELEASE_CUTLERY
        cutleryList.ifPresent { c -> CutleryPool.putCutlery(c) }
        cutleryList = Optional.empty()
    }

    private fun sleep() {
        status = Status.SLEEP
        sleepForAWhile()
    }

    fun sleepForAWhile() = Thread.sleep(1000 * (1 + java.lang.Math.random() * 10).toLong())

}
